class SimpleSearch:

    # 1. Determines the longest word within a given text. The words are separated with
    # spaces. If there are more than one word of maxim length, return the last one identified.
    @staticmethod
    def get_longest_word(input_string):
        if not (type(input_string) is str) or (input_string == ''):
            return 0

        str_list = input_string.split(' ')
        max_len = 0
        max_index = 0
        for index in range(0, len(str_list)):
            if len(str_list[index]) >= max_len:
                max_len = len(str_list[index])
                max_index = index
        return str_list[max_index]

    # Retrieves the frequency list of a list of any type.
    @staticmethod
    def get_list_freq(my_list):
        freq = {}
        for my_elem in my_list:
            freq[my_elem] = 0
        for my_elem in my_list:
            freq[my_elem] += 1

        return freq

    # 2. Determines the frequency of words within a text.
    @staticmethod
    def get_word_freq(input_string):
        if not (type(input_string) is str) or (input_string == ''):
            return {}

        str_list = input_string.split(' ')

        return SimpleSearch.get_list_freq(str_list)

    # 3. Determines the most frequent word from a text.
    @staticmethod
    def get_most_frequent_word(input_string):
        freq = SimpleSearch.get_word_freq(input_string)
        if freq == {}:
            return None

        return max(freq, key=freq.get)
        # alternatively, return utils.IntListHelper.get_max(freq.get)

    # Determines the list of n-grams generated from a single word.
    @staticmethod
    def get_ngrams_from_single_word(n, input_string):
        if not (type(input_string) is str) or (input_string == '') or \
                (not (type(n) is int) or n < 1 or n > len(input_string)):
            return []

        res = []
        aux = n
        for index in range(0, len(input_string)):
            tmp = input_string[index: aux]
            if len(tmp) == n and not (' ' in tmp):
                res.append(tmp)
                aux += 1

        return res

    # Determines the list of n-grams of a sentence. Ignores spaces.
    @staticmethod
    def get_ngrams_from_sentence(n, input_string):
        if not (type(input_string) is str) or (input_string == '') or \
                (not (type(n) is int) or n < 1 or n > len(input_string)):
            return []

        str_no_spaces = input_string.replace(' ', '')
        return SimpleSearch.get_ngrams_from_single_word(n, str_no_spaces)

    # 4. Determines the frequency of the n-grams (words composed from n letters, ignoring the
    # spaces) from a given text.
    @staticmethod
    def get_ngram_freq(n, input_string):
        if not (type(input_string) is str) or (input_string == '') or (not (type(n) is int) or n < 1):
            return {}

        ngram_list = SimpleSearch.get_ngrams_from_sentence(n, input_string)

        return SimpleSearch.get_list_freq(ngram_list)

    # 5. Determines the most frequent n-gram from a text.
    @staticmethod
    def get_most_frequent_ngram(n, input_string):
        if not (type(input_string) is str) or (input_string == '') or (not (type(n) is int) or n < 1):
            return None

        ngram_list = SimpleSearch.get_ngram_freq(n, input_string)
        return max(ngram_list, key=ngram_list.get)
