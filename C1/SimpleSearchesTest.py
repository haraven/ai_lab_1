import unittest
from SimpleSearches import *


class SimpleSearchTest(unittest.TestCase):
    def test_get_longest_word(self):
        test_sentence = "The bouquet has three tulips and thirteen freesias"
        res = SimpleSearch.get_longest_word(test_sentence)
        self.assertEqual(res, "freesias")

        test_sentence = "zeros one two three one fours two one fives"
        res = SimpleSearch.get_longest_word(test_sentence)
        self.assertNotEqual(res, "zeros")
        self.assertEqual(res, "fives")

        test_sentence = "abc deabcd cde"
        res = SimpleSearch.get_longest_word(test_sentence)
        self.assertNotEqual(res, "")
        self.assertEqual(res, "deabcd")

    def test_get_word_freq(self):
        test_sentence = "zero one two three one four two one five"
        res = SimpleSearch.get_word_freq(test_sentence)
        self.assertEqual(res, {"zero": 1, "one": 3, "two": 2, "three": 1, "four": 1, "five": 1})

        test_sentence = "a a b c d"
        res = SimpleSearch.get_word_freq(test_sentence)
        self.assertEqual(res, {"a": 2, "b": 1, "c": 1, "d": 1})

        test_sentence = ""
        res = SimpleSearch.get_word_freq(test_sentence)
        self.assertEqual(res, {})

        test_sentence = "a"
        res = SimpleSearch.get_word_freq(test_sentence)
        self.assertEqual(res, {"a": 1})

        test_sentence = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa a"
        res = SimpleSearch.get_word_freq(test_sentence)
        self.assertEqual(res, {"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa": 1, "a": 1})

    def test_get_most_frequent_word(self):
        test_sentence = "zero one two three one four two one five"
        res = SimpleSearch.get_most_frequent_word(test_sentence)
        self.assertEqual(res, "one")

        test_sentence = "a a a a b c"
        res = SimpleSearch.get_most_frequent_word(test_sentence)
        self.assertEqual(res, "a")

        test_sentence = ""
        res = SimpleSearch.get_most_frequent_word(test_sentence)
        self.assertEqual(res, None)

        test_sentence = "a"
        res = SimpleSearch.get_most_frequent_word(test_sentence)
        self.assertEqual(res, "a")

        test_sentence = "wordwordwordword word wordwordword wordword wordwordwordword word word"
        res = SimpleSearch.get_most_frequent_word(test_sentence)
        self.assertEqual(res, "word")

    def test_get_ngram_freq(self):
        n = 3
        test_sentence = "abc deabcd cde"
        res = SimpleSearch.get_ngram_freq(n, test_sentence)
        self.assertEqual(res, {"abc": 2, "bcd": 2, "cde": 2, "dea": 1, "eab": 1, "cdc": 1, "dcd": 1})

        n = 1
        res = SimpleSearch.get_ngram_freq(n, test_sentence)
        self.assertEqual(res, {"a": 2, "b": 2, "c": 3, "d": 3, "e": 2})

        n = 3
        test_sentence = "aaaa a aaa"
        res = SimpleSearch.get_ngram_freq(n, test_sentence)
        self.assertNotEqual(res, {"aaa": 2})
        self.assertEqual(res, {"aaa": 6})

        n = 0
        res = SimpleSearch.get_ngram_freq(n, test_sentence)
        self.assertEqual(res, {})

    def test_get_most_frequent_ngram(self):
        n = 3
        test_sentence = "abc deabcd cde"
        res = SimpleSearch.get_most_frequent_ngram(n, test_sentence)
        self.assertNotEqual(res, "dea")

        test_sentence = "abc deabcd cdeb cdac"
        res = SimpleSearch.get_most_frequent_ngram(n, test_sentence)
        self.assertEqual(res, "bcd")

        n = 1
        res = SimpleSearch.get_most_frequent_ngram(n, test_sentence)
        self.assertEqual(res, "c")

        n = 2
        test_sentence = "a a b a a aa b"
        res = SimpleSearch.get_most_frequent_ngram(n, test_sentence)
        self.assertEqual(res, "aa")

        test_sentence = ""
        res = SimpleSearch.get_most_frequent_ngram(n, test_sentence)
        self.assertEqual(res, None)


if __name__ == '__main__':
    unittest.main()
