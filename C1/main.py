from SimpleSearchesTest import *

if __name__ == "__main__":
    my_test = SimpleSearchTest()

    my_test.test_get_longest_word()
    my_test.test_get_word_freq()
    my_test.test_get_most_frequent_word()
    my_test.test_get_ngram_freq()
    my_test.test_get_most_frequent_ngram()
