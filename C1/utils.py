class IntListHelper:

    # Computes the max value of an int list
    @staticmethod
    def get_max(int_list):
        if not isinstance(int_list, list) or not all(isinstance(x, int) for x in int_list) or len(int_list) == 0:
            return None

        max_val = int_list[0]
        for index in range(0, len(int_list)):
            if int_list[index] > max_val:
                max_val = int_list[index]

        return max_val