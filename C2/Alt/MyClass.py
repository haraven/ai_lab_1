from enum import Enum


class MyClass:
    def __init__(self, girls, boys):
        self.girl_count = girls
        self.boy_count = boys


class StudentTypes(Enum):
    STUDENT_TYPE_GIRL = 0
    STUDENT_TYPE_BOY = 1
    STUDENT_TYPE_ALL = 2


class MyStudent:
    def __init__(self):
        self.class_name = None
        self.student_type = None

    def __repr__(self):
        return self.class_name + ("g" if self.student_type == StudentTypes.STUDENT_TYPE_GIRL else "b")
