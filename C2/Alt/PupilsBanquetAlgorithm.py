from MyClass import *


class PupilsBanquet:
    def __init__(self, k):
        self.classes = []
        self.class_count = k
        self.solution = []
        self.total_students = 0

    def get_total_entity_count(self, student_type):
        counter = 0
        for my_class in self.classes:
            if student_type == StudentTypes.STUDENT_TYPE_GIRL:
                counter += my_class.girl_count
            elif student_type == StudentTypes.STUDENT_TYPE_BOY:
                counter += my_class.boy_count
            else:
                counter += my_class.girl_count + my_class.boy_count

        return counter

    def read_classes(self, filename):
        file = open(filename)
        data = file.readline()
        self.class_count = int(data.split()[0])

        for i in range(self.class_count):
            line = file.readline()
            if line == "" or line == '\n':
                break
            else:
                data = line.split()
                tmp = MyClass(int(data[0]), int(data[1]))
                self.classes.append(tmp)

        if self.get_total_entity_count(StudentTypes.STUDENT_TYPE_GIRL) > self.get_total_entity_count(
                StudentTypes.STUDENT_TYPE_BOY):
            print("Warning: There are more girls than there are boys. Some girls will be left out!")

        self.total_students = self.get_total_entity_count(StudentTypes.STUDENT_TYPE_ALL)

        file.close()

    def get_class_with_most_students_of_type(self, student_type, exclude):
        if student_type == StudentTypes.STUDENT_TYPE_GIRL:
            max = 0
            max_index = -1
            for index in range(len(self.classes)):
                if self.classes[index].girl_count >= max and index != exclude:
                    max = self.classes[index].girl_count
                    max_index = index
            return max_index
        else:
            max = 0
            max_index = -1
            for index in range(len(self.classes)):
                if self.classes[index].boy_count >= max and index != exclude:
                    max = self.classes[index].boy_count
                    max_index = index
            return max_index

    def has_any_students_of_type(self, student_type):
        return self.get_total_entity_count(student_type) != 0

    def class_has_any_students_of_type(self, student_type, class_index):
        if student_type == StudentTypes.STUDENT_TYPE_GIRL:
            return self.classes[class_index].girl_count != 0
        else:
            return self.classes[class_index].boy_count != 0

    def are_all_students_in_one_class(self):
        for my_class in self.classes:
            if my_class.boy_count + my_class.girl_count == self.get_total_entity_count(StudentTypes.STUDENT_TYPE_ALL):
                return True
        return False

    def is_valid_student(self, partial_solution, student):
        if len(partial_solution) == 0:
            return True

        last_student = partial_solution[len(partial_solution) - 1]
        if student.class_name == last_student.class_name:
            return False

        if len(partial_solution) == self.total_students - 1:
            first_student = partial_solution[0]
            if first_student.student_type == StudentTypes.STUDENT_TYPE_GIRL \
                    and student.student_type == StudentTypes.STUDENT_TYPE_GIRL:
                return False
            elif first_student.class_name == student.class_name:
                return False
            if last_student.student_type == StudentTypes.STUDENT_TYPE_GIRL \
                    and student.student_type == StudentTypes.STUDENT_TYPE_GIRL:
                return False

        else:
            if self.has_any_students_of_type(StudentTypes.STUDENT_TYPE_GIRL):
                if student.student_type == last_student.student_type:
                    return False
        return True

    def update_class_student_count(self, my_student, index, operation):
        if my_student.student_type == StudentTypes.STUDENT_TYPE_GIRL:
            if operation == "-":
                self.classes[index].girl_count -= 1
            else:
                self.classes[index].girl_count += 1
        else:
            if operation == "+":
                self.classes[index].boy_count += 1
            else:
                self.classes[index].boy_count -= 1

    def has_any_students(self, class_id):
        return self.classes[class_id].girl_count != 0 or self.classes[class_id].boy_count != 0

    def is_solution(self, partial_solution):
        if len(partial_solution) == self.total_students:
            return True

        if not (self.has_any_students_of_type(StudentTypes.STUDENT_TYPE_BOY)):
            return True

        return False

    def __solve(self, partial_solution, last_visited, last_type):
        if self.is_solution(partial_solution):
            self.solution = partial_solution.copy()
            return

        if last_type == StudentTypes.STUDENT_TYPE_BOY:
            if self.has_any_students_of_type(StudentTypes.STUDENT_TYPE_GIRL):
                class_index = self.get_class_with_most_students_of_type(StudentTypes.STUDENT_TYPE_GIRL, last_visited)
                if class_index >= 0:
                    student = MyStudent()
                    student.student_type = StudentTypes.STUDENT_TYPE_GIRL
                    student.class_name = "Class " + str(class_index)
                    if self.is_valid_student(partial_solution, student):
                        last_visited = class_index
                        last_type = StudentTypes.STUDENT_TYPE_GIRL
                        partial_solution.append(student)
                        self.update_class_student_count(student, class_index, "-")
                        self.__solve(partial_solution, last_visited, last_type)
            else:
                class_index = self.get_class_with_most_students_of_type(StudentTypes.STUDENT_TYPE_BOY, last_visited)
                if class_index >= 0:
                    student = MyStudent()
                    student.student_type = StudentTypes.STUDENT_TYPE_BOY
                    student.class_name = "Class " + str(class_index)
                    if self.is_valid_student(partial_solution, student):
                        last_visited = class_index
                        last_type = StudentTypes.STUDENT_TYPE_BOY
                        partial_solution.append(student)
                        self.update_class_student_count(student, class_index, "-")
                        self.__solve(partial_solution, last_visited, last_type)

        else:
            class_index = self.get_class_with_most_students_of_type(StudentTypes.STUDENT_TYPE_BOY, last_visited)
            if class_index >= 0:
                student = MyStudent()
                student.student_type = StudentTypes.STUDENT_TYPE_BOY
                student.class_name = "Class " + str(class_index)
                if self.is_valid_student(partial_solution, student):
                    last_visited = class_index
                    last_type = StudentTypes.STUDENT_TYPE_BOY
                    partial_solution.append(student)
                    self.update_class_student_count(student, class_index, "-")
                    self.__solve(partial_solution, last_visited, last_type)

    def solve(self):
        # self.__solve([], -1, StudentTypes.STUDENT_TYPE_BOY)
        # visited = [False] * self.class_count
        self.__solve_all([], -1)
        return self.solution

    def is_solution_all(self, partial_solution):
        if len(partial_solution) == self.total_students:
            for index in range (1, len(partial_solution)):
                if partial_solution[index - 1].class_name == partial_solution[index].class_name:
                    return False
                if partial_solution[index - 1].student_type == StudentTypes.STUDENT_TYPE_GIRL \
                        and partial_solution[index].student_type == StudentTypes.STUDENT_TYPE_GIRL:
                    return False
            return True
        else:
            return False

    def __solve_all(self, partial_solution, visited):
        if self.is_solution_all(partial_solution):
            self.solution.append(partial_solution.copy())
            return
        else:
            for class_index in range(self.class_count):
                if visited != class_index:
                    if self.has_any_students(class_index):
                        student = MyStudent()
                        student.class_name = "C" + str(class_index)
                        if self.class_has_any_students_of_type(StudentTypes.STUDENT_TYPE_GIRL, class_index):
                            student.student_type = StudentTypes.STUDENT_TYPE_GIRL
                            if self.is_valid_student(partial_solution, student):
                                partial_solution.append(student)
                                visited = class_index
                                self.update_class_student_count(student, class_index, "-")
                                self.__solve_all(partial_solution, visited)
                                self.update_class_student_count(student, class_index, "+")
                                partial_solution.pop()
                                visited = -1
                        if self.class_has_any_students_of_type(StudentTypes.STUDENT_TYPE_BOY, class_index):
                            student.student_type = StudentTypes.STUDENT_TYPE_BOY
                            if self.is_valid_student(partial_solution, student):
                                partial_solution.append(student)
                                visited = class_index
                                self.update_class_student_count(student, class_index, "-")
                                self.__solve_all(partial_solution, visited)
                                self.update_class_student_count(student, class_index, "+")
                                partial_solution.pop()
                                visited = -1
