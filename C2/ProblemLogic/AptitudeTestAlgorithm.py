import datetime
import time

import collections


class AptitudeTestAlgorithm:
    def __init__(self, questions_lower_limit, questions_upper_limit, points_min, points_max):
        self.total_questions = 0
        self.question_marks = []
        self.solutions = []
        self.pts_x = points_min
        self.pts_y = points_max
        if self.pts_x > self.pts_y:
            self.pts_x, self.pts_y = self.pts_y, self.pts_x

        self.questions_u = questions_lower_limit
        self.questions_v = questions_upper_limit
        if self.questions_u > self.questions_v:
            self.questions_u, self.questions_v = self.questions_v, self.questions_u

    def read(self, filename):
        file = open(filename)
        line = file.readline()
        if line != '':
            self.total_questions = int(line)
        else:
            return
        for index in range(self.total_questions):
            line = file.readline()
            if line != '':
                self.question_marks.append(int(line))
            else:
                continue

        file.close()

    def clear_solutions(self):
        self.solutions.clear()

    def is_valid_question(self, partial_solution, question):
        if sum(partial_solution) + question > self.pts_y:
            return False
        if len(partial_solution) + 1 > self.questions_v:
            return False

        return True

    def is_solution(self, partial_solution):
        if len(partial_solution) == 0:
            return False
        res_sum = sum(partial_solution)
        if self.pts_x <= res_sum <= self.pts_y \
                and self.questions_u <= len(partial_solution) <= self.questions_v:
            return True

        return False

    def __solve(self, partial_solution, visited):
        if self.is_solution(partial_solution):
            self.solutions.append(partial_solution.copy())
        for index in range(self.total_questions):
            if not (visited[index]) and self.is_valid_question(partial_solution, self.question_marks[index]):
                visited[index] = True
                partial_solution.append(self.question_marks[index])
                self.__solve(partial_solution, visited)
                visited[index] = False
                partial_solution.pop()

    def solve(self):
        start = time.time()
        visited = [False] * self.total_questions
        self.__solve([], visited)
        end = time.time()
        start_datetime = datetime.datetime.fromtimestamp(start)
        end_datetime = datetime.datetime.fromtimestamp(end)
        return self.solutions, start_datetime, end_datetime
