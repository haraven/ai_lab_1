import sys
import threading

from ProblemLogic.AptitudeTestAlgorithm import *
from Tests.AptitudeTestAlgTest import *


def print_solutions_hr(my_quiz):
    solutions, start_time, end_time = my_quiz.solve()
    print("Printing all question combinations that have between " + str(my_quiz.questions_u) + " and " + str(
        my_quiz.questions_v) + " questions, and between " + str(my_quiz.pts_x) + " and " + str(
        my_quiz.pts_y) + " points:")
    if len(solutions) == 0:
        print("There are no solutions.")
    print()
    for sol_index in range(len(solutions)):
        print("Questionnaire " + str(sol_index), end=":\n")
        for index in range(len(solutions[sol_index])):
            if index != len(solutions[sol_index]) - 1:
                print("Q" + str(index) + ": " + str(solutions[sol_index][index]), end=", ")
            else:
                print("Q" + str(index) + ": " + str(solutions[sol_index][index]))
        print("---")

    print("Started at: " + str(start_time.strftime('%H:%M:%S')) + ". Ended at: " + str(
        end_time.strftime('%H:%M:%S')) + ". Duration: " + str(end_time - start_time) + ".")


def get_user_input():
    questions_lower_limit = int(input("Enter the least amount of questions.\n> "))
    questions_upper_limit = int(input("Enter the most amount of questions.\n> "))
    questions_min_pts = int(input("Enter the least amount of points for each questionnaire.\n> "))
    questions_max_pts = int(input("Enter the most amount of points for each questionnaire.\n> "))

    return questions_lower_limit, questions_upper_limit, questions_min_pts, questions_max_pts


def main():
    print("Running tests...")
    my_test = AptitudeTestAlgTest()
    my_test.test()
    print("Tests finished successfully.")

    ql, qu, pl, pu = get_user_input()
    my_quiz = AptitudeTestAlgorithm(ql, qu, pl, pu)
    filename = input(
        "Enter a file to read data from (file must start with total no. of questions, then the amount of pts for each question on its own line.\n> ")
    my_quiz.read(filename)
    print_solutions_hr(my_quiz)


threading.stack_size(67108864)
sys.setrecursionlimit(2 ** 20)
thread = threading.Thread(target=main)
thread.start()
