from ProblemLogic.AptitudeTestAlgorithm import *


class AptitudeTestAlgTest:
    def __init__(self):
        self.questions_ll = 1
        self.questions_ul = 1
        self.points_ll = 1
        self.points_ul = 1

    def test(self):
        my_quiz = AptitudeTestAlgorithm(self.questions_ll, self.questions_ul, self.points_ll, self.points_ul)
        my_quiz.question_marks = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
        my_quiz.total_questions = len(my_quiz.question_marks)
        my_quiz.clear_solutions()
        sol, start, end = my_quiz.solve()
        assert [1] in sol
        self.points_ul = 3
        self.questions_ul = 2
        my_quiz.pts_y = self.points_ul
        my_quiz.questions_v = self.questions_ul
        my_quiz.clear_solutions()
        sol, start, end = my_quiz.solve()
        assert [1, 2] in sol
        assert [3] in sol
        assert [2, 1] in sol

        self.points_ul = 0
        self.points_ll = 0
        my_quiz.pts_y = self.points_ul
        my_quiz.pts_x = self.points_ll
        my_quiz.clear_solutions()
        sol, start, end = my_quiz.solve()
        assert len(sol) == 0

        self.points_ll = 0
        self.points_ul = 100000
        self.questions_ul = 0
        self.questions_ll = 0
        my_quiz.pts_x = self.points_ll
        my_quiz.pts_y = self.points_ul
        my_quiz.questions_u = self.questions_ul
        my_quiz.questions_v = self.questions_ll
        my_quiz.clear_solutions()
        sol, start, end = my_quiz.solve()
        assert len(sol) == 0

        self.questions_ul = 5
        my_quiz.questions_v = self.questions_ul
        my_quiz.clear_solutions()
        sol, start, end = my_quiz.solve()
        assert [1, 2, 3, 4, 5] in sol
