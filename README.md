C1. Simple search procedures


Implement (in Python) a simple algorithm for each of the following requests. The algorithms
must respect the specifications and will be evaluated using 5 assertions.


1. Please determine the longest word within a given text. The words are separated with
spaces. If there are more than one word of maxim length, return the last one identified.
Example:
In: ”The bouquet has three tulips and thirteen freesias”
Out: ”freesias”
Points: 5p for the code and 5x1p for the assertions


2. Please determine the frequency of words within a text.
Example:
In: ”zero one two three one four two one five”
Out: ”zero”:1, ”one”:3, ”two”:2, ”three”:1, ”four”:1, ”five”:1
Points: 5p for code and 5x1p for the assertions


3. Please determine the most frequent word from a text.
1 Artificial Intelligence, 2015-2016
Assignment 1 Uninformed search methods
Example:
In: ”zero one two three one four two one five”
Out: ”one”:3
Points: 5p for code and 5x1p for the assertions


4. Determine the frequency of the n-grams (words composed from n letters, ignoring the
spaces) from a given text.
Example:
In: ”abc deabcd cde”, n = 3
Out: ”abc”:2, ”bcd”:2, ”cde”:2, ”dea”:1, ”eab”:1, ”cdc”:1, ”dcd”:1
Points: 15p for code and 5x3p for the assertions


5. Determine the most frequent n-gram from a text.
Example:
In: ”abc deabcd cdeb cda”, n = 3
Out: ”bcd”
Points: 15p for code and 5x3p for the assertions

C2. Search problems
13. Aptitude test
A set of n questions are available in order to develop an aptitude test, question i being
rated with pi points. It is required to develop all questionnaires having the total
number of questions between u and v and having the total number of points between x and y.